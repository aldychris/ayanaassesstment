# AYANA Code Assesstment

A new Flutter for AYANA assesstment.

## Getting Started

This project build on flutter version 3.13.4

Image source from this apps is get from [PicSum](https://picsum.photos/)

## How to run this project
If you're using fvm then you can run this project by using this command

``` shell
fvm install 3.13.4
fvm use 3.13.4
```

``` shell
fvm flutter run -t lib/main_staging.dart -d chrome --web-renderer canvaskit --release
```

If you're not using fvm then you can run this project by using this command

``` shell
flutter run -t lib/main_staging.dart -d chrome --web-renderer canvaskit --release
```

### Caveat
The images for this app are sourced from [PicSum](https://picsum.photos/) to provide a more realistic simulation.

For state management, this project utilizes  [Flutter BLOC](https://pub.dev/packages/flutter_bloc) state management and and the carousel functionality on the home page is achieved using the [carousel_slider](https://pub.dev/packages/carousel_slider) package .


Originally, the Carrousel part written in vanilla Flutter code, which is still available but has been commented out. 

The decision to adopt the carousel_slider package was made after performance evaluations showed better result, hence the carousel_slider is currently in use. As comparison using vanilla is achived 50-60fps while scrolling and the carousel_slider is able to achieve 30-50fps while scrolling.

Further performance enhancements may also be explored to optimize the performance on the vanilla code it's essential to note that retaining the vanilla code  allows for easier customization in the future if required.

Efforts have been made to maximize the responsiveness of the home page, aligning with the guidelines outlined in Material 3's [Adaptive Layout](https://m3.material.io/foundations/layout/understanding-layout/overview)

Please note that the arrow buttons beside the carousel in desktop mode on the web are non-functional as they do not currently have associated functions.
