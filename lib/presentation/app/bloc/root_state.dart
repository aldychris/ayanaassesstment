abstract class RootState {
  RootState(this.index);
  int index;
}

class ShowHomePage extends RootState {
  ShowHomePage(super.index);
}

class ShowChatPage extends RootState {
  ShowChatPage(super.index);
}

class ShowMapPage extends RootState {
  ShowMapPage(super.index);
}

class ShowProfilePage extends RootState {
  ShowProfilePage(super.index);
}
