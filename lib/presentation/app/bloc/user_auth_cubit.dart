import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'user_auth_state.dart';

///All related to session and user access
class UserAuthCubit extends Cubit<UserAuthState> {
  UserAuthCubit() : super(UserAuthInitial());

  void logout() {}
}
