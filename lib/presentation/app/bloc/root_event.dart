import 'package:meta/meta.dart';

@immutable
abstract class RootEvent {}

class ChangeRootPage extends RootEvent {
  ChangeRootPage(this.index);
  final int index;
}
