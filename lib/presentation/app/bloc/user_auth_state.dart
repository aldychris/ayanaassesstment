part of 'user_auth_cubit.dart';

@immutable
abstract class UserAuthState {}

class UserAuthInitial extends UserAuthState {}

class LoginSuccess extends UserAuthState {}

class LoginFailed extends UserAuthState {
  LoginFailed({required this.message});
  final String message;
}
