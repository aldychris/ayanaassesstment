import 'package:ayana_stg/presentation/app/bloc/root_event.dart';
import 'package:ayana_stg/presentation/app/bloc/root_state.dart';
import 'package:bloc/bloc.dart';

class RootBloc extends Bloc<RootEvent, RootState> {
  RootBloc() : super(ShowHomePage(0)) {
    on<ChangeRootPage>(_changeRootPage);
  }

  void _changeRootPage(ChangeRootPage event, Emitter<RootState> emit) {
    switch (event.index) {
      case 0:
        emit(ShowHomePage(event.index));
      case 1:
        emit(ShowChatPage(event.index));
      case 2:
        emit(ShowMapPage(event.index));
      case 3:
        emit(ShowProfilePage(event.index));
    }
  }
}
