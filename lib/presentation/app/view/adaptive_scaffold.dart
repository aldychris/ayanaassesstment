import 'package:ayana_stg/core/di/dependency_inversion.dart';
import 'package:ayana_stg/core/themes/index.dart';
import 'package:ayana_stg/core/utils/device_screen_helper.dart';
import 'package:flutter/material.dart';

/// See bottomNavigationBarItem or NavigationRailDestination
class AdaptiveScaffoldDestination {
  const AdaptiveScaffoldDestination({
    required this.title,
    required this.icon,
  });

  final String title;
  final IconData icon;
}

/// A widget that adapts to the current display size, displaying a [Drawer],
/// [NavigationRail], or [BottomNavigationBar]. Navigation destinations are
/// defined in the [destinations] parameter.
class AdaptiveScaffold extends StatefulWidget {
  const AdaptiveScaffold({
    required this.currentIndex,
    required this.destinations,
    this.title,
    this.body,
    this.actions = const [],
    this.onNavigationIndexChange,
    this.onTrailingIconTap,
    super.key,
  });

  final Widget? title;
  final List<Widget> actions;
  final Widget? body;
  final int currentIndex;
  final List<AdaptiveScaffoldDestination> destinations;
  final ValueChanged<int>? onNavigationIndexChange;
  final VoidCallback? onTrailingIconTap;

  @override
  State<AdaptiveScaffold> createState() => _AdaptiveScaffoldState();
}

class _AdaptiveScaffoldState extends State<AdaptiveScaffold> {
  @override
  Widget build(BuildContext context) {
    final screenSize = sl<DeviceScreenHelper>()
        .getDeviceScreenSize(MediaQuery.of(context).size);
    switch (screenSize) {
      case DeviceScreenSize.smallScreen:
        // Show a bottom app bar
        return _buildSmallScaffold();
      case DeviceScreenSize.mediumScreen:
        // Show a navigation rail
        return _buildNavigationRail(false);
      case DeviceScreenSize.largeScreen:
        // Show a Drawer
        return _buildNavigationRail(true);
    }
  }

  Widget _buildSmallScaffold() {
    return Scaffold(
      body: widget.body,
      backgroundColor: AyanaColors.secondary,
      bottomNavigationBar: NavigationBar(
        backgroundColor: Colors.white54,
        height: 80,
        elevation: 1,
        indicatorColor: AyanaColors.accent,
        onDestinationSelected: (int index) {
          widget.onNavigationIndexChange?.call(index);
        },
        selectedIndex: widget.currentIndex,
        destinations: [
          ...widget.destinations.map(
            (d) {
              return NavigationDestination(
                icon: Icon(d.icon),
                label: d.title,
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildNavigationRail(bool extended) {
    return Scaffold(
      backgroundColor: AyanaColors.secondaryAccent,
      body: Row(
        children: [
          NavigationRail(
            minExtendedWidth: 160,
            backgroundColor: AyanaColors.secondaryAccent,
            selectedLabelTextStyle: const TextStyle(
              color: Colors.black,
              backgroundColor: AyanaColors.primary,
            ),
            selectedIconTheme: const IconThemeData(
              color: Colors.black,
            ),
            unselectedIconTheme: const IconThemeData(
              color: Colors.grey,
            ),
            leading: _buildLeading(),
            groupAlignment: -0.7,
            extended: extended,
            elevation: 0.1,
            destinations: [
              ...widget.destinations.map(
                (d) {
                  return NavigationRailDestination(
                    padding: EdgeInsets.zero,
                    selectedIcon: Center(
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Icon(
                          d.icon,
                          size: 42,
                        ),
                      ),
                    ),
                    icon: Container(
                      padding: const EdgeInsets.all(8),
                      child: Container(
                        padding: const EdgeInsets.all(4),
                        child: Icon(
                          d.icon,
                          size: 42,
                        ),
                      ),
                    ),
                    label: Text(
                      d.title,
                    ),
                  );
                },
              ),
            ],
            selectedIndex: widget.currentIndex,
            onDestinationSelected: widget.onNavigationIndexChange ?? (_) {},
          ),
          Expanded(
            child: widget.body!,
          ),
        ],
      ),
    );
  }

  Widget _buildLeading() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 72, 16, 32),
      child: Image.asset(
        'assets/images/ayanalogo.png',
        height: 92,
        width: 92,
      ),
    );
  }

  // ignore: unused_element
  Widget _buildLargeScaffold() {
    return Row(
      children: [
        Drawer(
          child: Column(
            children: [
              DrawerHeader(
                child: Center(
                  child: _buildLeading(),
                ),
              ),
              for (final d in widget.destinations)
                ListTile(
                  leading: Icon(d.icon),
                  title: Text(d.title),
                  selected:
                      widget.destinations.indexOf(d) == widget.currentIndex,
                  onTap: () => _destinationTapped(d),
                ),
            ],
          ),
        ),
        VerticalDivider(
          width: 1,
          thickness: 1,
          color: Colors.grey[300],
        ),
        Expanded(
          child: Scaffold(
            body: widget.body,
          ),
        ),
      ],
    );
  }

  void _destinationTapped(AdaptiveScaffoldDestination destination) {
    final idx = widget.destinations.indexOf(destination);
    if (idx != widget.currentIndex) {
      widget.onNavigationIndexChange!(idx);
    }
  }
}
