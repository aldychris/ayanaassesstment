import 'package:ayana_stg/core/coordinator/index.dart';
import 'package:ayana_stg/core/di/dependency_inversion.dart';
import 'package:flutter/material.dart';

class AyanaAppBar extends StatelessWidget implements PreferredSizeWidget {
  // ignore: use_super_parameters,prefer_const_constructors_in_immutables
  AyanaAppBar({
    required this.automaticallyImplyLeading,
    required this.title,
    Key? key,
    this.backButtonCallback,
    this.leading,
    this.actions,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  final bool automaticallyImplyLeading;
  final VoidCallback? backButtonCallback;
  final Widget? leading;
  final String title;
  final List<Widget>? actions;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: automaticallyImplyLeading,
      elevation: 0,
      title: Text(
        title,
        style: const TextStyle(color: Colors.grey),
      ),
      leading: _buildLeading(context),
      leadingWidth: 56,
      actions: actions,
      backgroundColor: Colors.white,
    );
  }

  Widget? _buildLeading(BuildContext context) {
    if (leading != null) {
      return leading;
    }
    return automaticallyImplyLeading && sl.get<Coordinator>().canPop(context)
        ? IconButton(
            onPressed: () {
              if (backButtonCallback != null) {
                return backButtonCallback?.call();
              } else {
                sl.get<Coordinator>().pop(context);
              }
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
              size: 24,
            ),
          )
        : const SizedBox.shrink();
  }
}
