import 'package:ayana_stg/core/di/dependency_inversion.dart';
import 'package:ayana_stg/core/routes/ayana_router.dart';
import 'package:ayana_stg/core/themes/ayana_theme.dart';
import 'package:ayana_stg/presentation/app/bloc/user_auth_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserAuthCubit(),
      child: ScreenUtilInit(
        designSize: const Size(360, 640),
        minTextAdapt: true,
        builder: (_, child) {
          return MaterialApp.router(
            theme: AyanaThemeData.lightTheme,
            routerDelegate: sl.get<AyanaRouter>().delegate(),
            routeInformationParser: sl.get<AyanaRouter>().defaultRouteParser(),
          );
        },
      ),
    );
  }
}
