import 'package:auto_route/auto_route.dart';
import 'package:ayana_stg/core/di/dependency_inversion.dart';
import 'package:ayana_stg/presentation/app/bloc/index.dart';
import 'package:ayana_stg/presentation/app/view/adaptive_scaffold.dart';
import 'package:ayana_stg/presentation/chat/view/chat_page.dart';
import 'package:ayana_stg/presentation/home/view/home_page.dart';
import 'package:ayana_stg/presentation/map/view/map_page.dart';
import 'package:ayana_stg/presentation/profile/view/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage()
class RootPage extends StatefulWidget implements AutoRouteWrapper {
  const RootPage({super.key});

  static const String routeName = '/';

  @override
  _RootPageState createState() => _RootPageState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => sl.get<RootBloc>(),
          child: this,
        ),
        BlocProvider.value(
          value: sl.get<UserAuthCubit>(),
          child: this,
        ),
      ],
      child: this,
    );
  }
}

class _RootPageState extends State<RootPage> {
  late PageController _controller;
  late RootBloc _rootBloc;
  late UserAuthCubit _userAuthCubit;

  @override
  void initState() {
    _controller = PageController();
    _rootBloc = BlocProvider.of<RootBloc>(context);
    _userAuthCubit = BlocProvider.of<UserAuthCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocSelector<RootBloc, RootState, int>(
      selector: (state) => state.index,
      builder: (context, state) {
        return AdaptiveScaffold(
          currentIndex: state,
          onNavigationIndexChange: (index) {
            _rootBloc.add(ChangeRootPage(index));
            _controller.jumpToPage(index);
          },
          onTrailingIconTap: () {
            _userAuthCubit.logout();
          },
          destinations: const [
            AdaptiveScaffoldDestination(
              title: 'Home',
              icon: Icons.home,
            ),
            AdaptiveScaffoldDestination(
              title: 'Chat',
              icon: Icons.chat_bubble_outline_sharp,
            ),
            AdaptiveScaffoldDestination(
              title: 'Map',
              icon: Icons.map,
            ),
            AdaptiveScaffoldDestination(
              title: 'Profile',
              icon: Icons.person_outline_sharp,
            ),
          ],
          body: PageView.custom(
            controller: _controller,
            physics: const NeverScrollableScrollPhysics(),
            childrenDelegate: SliverChildBuilderDelegate(
              (context, index) {
                switch (index) {
                  case 0:
                    return const HomePage();
                  case 1:
                    return const ChatPage();
                  case 2:
                    return const MapPage();
                  case 3:
                    return const ProfilePage();
                  default:
                    return const ProfilePage();
                }
              },
              childCount: 4,
            ),
          ),
        );
      },
    );
  }
}
