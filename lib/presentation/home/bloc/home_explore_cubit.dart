import 'package:ayana_stg/core/model/home/category_model.dart';
import 'package:ayana_stg/core/model/home/explore_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_explore_state.dart';

class HomeExploreCubit extends Cubit<HomeExploreState> {
  HomeExploreCubit() : super(HomeExploreInitial());

  int activePage = 0;

  List<ExploreModel> bucketList = [
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=1',
      title: 'Sunrise Yoga',
      subtitle:
          'Sun salutaion paired with the sweet sound of morning biirds and '
          'crashing waves',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=2',
      title: 'Rock Bar',
      subtitle:
          'Cocktail amids 180 degrees of crashing waves, jagged limestone '
          'cliffs, annd the best sunset in Bali',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=3',
      title: 'Thalassotherapy Pool',
      subtitle:
          'Float move or exercise your way through series of hydromessage '
          'stations.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=4',
      title: 'Ocean Beach Pool',
      subtitle:
          'Beachfron infinity pool. Ocean smells. Crashing waves. Take the best'
          ' view in Bali.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=5',
      title: 'Farm to Bar Workshop',
      subtitle: 'Moons, gurus, and abstruse seekers will always protect them.',
    ),
  ];
  List<ExploreModel> kidsList = [
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=6',
      title: 'Balinese Dress Up',
      subtitle: 'The parrot pulls with love, ransack the cook islands until it '
          'travels.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=7',
      title: 'Guidepost at AYANA',
      subtitle: 'Rough, sunny jacks awkwardly taste a black, undead shore. '
          'cliffs, annd the best sunset in Bali',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=8',
      title: 'Green Camp AYANA',
      subtitle: 'Dark, rainy waves fiery scrape a coal-black, dead tuna. '
          'stations.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=9',
      title: 'Rediscover Agliculture',
      subtitle:
          'The whale fears with madness, crush the fortress until it stutters. '
          ' view in Bali.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=10',
      title: 'RIMBA Pool Slided',
      subtitle: 'Moons, gurus, and abstruse seekers will always protect them.',
    ),
  ];
  List<ExploreModel> wellnessList = [
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=11',
      title: 'Traditional Balinese Massage',
      subtitle: 'The parrot pulls with love, ransack the cook islands until it '
          'travels.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=12',
      title: 'Sound Healing',
      subtitle: 'Rough, sunny jacks awkwardly taste a black, undead shore. '
          'cliffs, annd the best sunset in Bali',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=13',
      title: 'Aqua Pilates',
      subtitle: 'Dark, rainy waves fiery scrape a coal-black, dead tuna. '
          'stations.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=14',
      title: 'Tennis',
      subtitle:
          'The whale fears with madness, crush the fortress until it stutters. '
          ' view in Bali.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=15',
      title: 'Healing Meditation',
      subtitle: 'Moons, gurus, and abstruse seekers will always protect them.',
    ),
  ];
  List<ExploreModel> romanticList = [
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=16',
      title: 'Pesta Lobster',
      subtitle: 'Try jumbling the cream margerines with shredded tabasco and '
          'red wine, refrigerated. ',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=17',
      title: 'Floating Brunch',
      subtitle: 'Dark, ripe pudding is best rinsed with old '
          'worcestershire sauce.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=18',
      title: 'Kubu Picnic',
      subtitle: 'Dark, rainy waves fiery scrape a coal-black, dead tuna. '
          'stations.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=19',
      title: 'AYANA Memories',
      subtitle:
          'The whale fears with madness, crush the fortress until it stutters. '
          ' view in Bali.',
    ),
    const ExploreModel(
      imgUrl: 'https://picsum.photos/768/1080?random=20',
      title: 'Afternoon Tea at AYANA Farm',
      subtitle: 'Moons, gurus, and abstruse seekers will always protect them.',
    ),
  ];

  List<CategoryModel> categoryList = [
    const CategoryModel(
      name: 'Bucket List',
      selected: true,
    ),
    const CategoryModel(
      name: 'Kids',
      selected: false,
    ),
    const CategoryModel(
      name: 'Wellness',
      selected: false,
    ),
    const CategoryModel(
      name: 'Romantic',
      selected: false,
    ),
  ];

  Future<void> getExploreCategory() async {
    emit(
      ExploreGetCategorySuccess(
        category: categoryList,
      ),
    );
  }

  Future<void> getExploreList(String category) async {
    emit(LoadingExploreInitial());

    categoryList = categoryList.map((e) {
      return e.copyWith(selected: e.name == category);
    }).toList();

    emit(ExploreCategoryChanged(
      category: categoryList,
    ));

    switch (category) {
      case 'Bucket List':
        emit(
          HomeExploreSuccess(
            exploreList: bucketList,
          ),
        );
      case 'Kids':
        emit(
          HomeExploreSuccess(
            exploreList: kidsList,
          ),
        );
      case 'Wellness':
        emit(
          HomeExploreSuccess(
            exploreList: wellnessList,
          ),
        );
      case 'Romantic':
        emit(
          HomeExploreSuccess(
            exploreList: romanticList,
          ),
        );
      default:
        emit(HomeExploreError());
    }
  }
}
