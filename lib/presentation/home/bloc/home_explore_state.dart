part of 'home_explore_cubit.dart';

abstract class HomeExploreState extends Equatable {
  const HomeExploreState();
}

class HomeExploreInitial extends HomeExploreState {
  @override
  List<Object> get props => [];
}

class LoadingExploreInitial extends HomeExploreState {
  @override
  List<Object> get props => [];
}

class ExploreGetCategorySuccess extends HomeExploreState {
  const ExploreGetCategorySuccess({
    required this.category,
  });

  final List<CategoryModel> category;

  @override
  List<Object> get props => [category];
}

class ExploreCategoryChanged extends HomeExploreState {
  const ExploreCategoryChanged({
    required this.category,
  });

  final List<CategoryModel> category;

  @override
  List<Object> get props => [category];
}

class HomeExploreSuccess extends HomeExploreState {
  const HomeExploreSuccess({
    required this.exploreList,
  });

  final List<ExploreModel> exploreList;

  @override
  List<Object> get props => [
        exploreList,
      ];
}

class HomeExploreError extends HomeExploreState {
  @override
  List<Object> get props => [];
}
