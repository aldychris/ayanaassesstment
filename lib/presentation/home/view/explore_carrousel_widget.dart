// ignore_for_file: prefer_const_constructors

import 'package:ayana_stg/core/model/home/category_model.dart';
import 'package:ayana_stg/presentation/home/bloc/home_explore_cubit.dart';
import 'package:ayana_stg/presentation/home/view/explore_slider_item_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loop_page_view/loop_page_view.dart';

class ExploreCarrouselWidget extends StatefulWidget {
  const ExploreCarrouselWidget({super.key});

  @override
  _ExploreCarrouselWidgetState createState() => _ExploreCarrouselWidgetState();
}

class _ExploreCarrouselWidgetState extends State<ExploreCarrouselWidget> {
  LoopScrollMode selectedScrollMode = LoopScrollMode.shortest;
  late LoopPageController pageController;
  int activePage = 0;

  late HomeExploreCubit _exploreCubit;

  @override
  void initState() {
    super.initState();
    pageController = LoopPageController(
      viewportFraction: 0.65,
    );
    _exploreCubit = BlocProvider.of<HomeExploreCubit>(context);
    _exploreCubit.getExploreCategory();
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 80,
          child: BlocBuilder<HomeExploreCubit, HomeExploreState>(
            buildWhen: (p, c) =>
                c is ExploreCategoryChanged || c is ExploreGetCategorySuccess,
            builder: (context, state) {
              var category = <CategoryModel>[];
              if (state is ExploreGetCategorySuccess) {
                category = state.category;
                _exploreCubit.getExploreList(state.category[0].name);
              } else if (state is ExploreCategoryChanged) {
                category = state.category;
              }

              return ListView.separated(
                itemCount: category.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return _buildCategoryMenu(category[index]);
                },
                separatorBuilder: (context, index) {
                  return _buildSeparator();
                },
              );
            },
          ),
        ),
        _buildResponsiveCarrouselSlider(),
        // _buildResponsiveCarrouselPageViewer(),
      ],
    );
  }

  Widget _buildCategoryMenu(CategoryModel category) {
    return InkWell(
      onTap: () {
        _exploreCubit.getExploreList(category.name);
      },
      child: Text(
        category.name.toUpperCase(),
        style: TextStyle(
          fontSize: 22,
          fontWeight: category.selected ? FontWeight.bold : FontWeight.normal,
          color: category.selected ? Colors.black : Colors.grey,
        ),
      ),
    );
  }

  Widget _buildSeparator() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        '/',
        style: TextStyle(
          fontSize: 22,
        ),
      ),
    );
  }

  //region Use Page Viewer
  Widget _buildResponsiveCarrouselPageViewer() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final w = constraints.maxWidth;
        if (w < 1000) {
          return _buildCarrouselPageViewer(w);
        } else {
          return Row(
            children: [
              Expanded(
                child: Icon(Icons.arrow_back),
              ),
              Expanded(
                flex: 8,
                child: _buildCarrouselPageViewer(w),
              ),
              Expanded(
                child: Icon(Icons.arrow_forward),
              ),
            ],
          );
        }
      },
    );
  }

  Widget _buildCarrouselPageViewer(double width) {
    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.touch,
          PointerDeviceKind.mouse,
        },
      ),
      child: AspectRatio(
        aspectRatio: (width / 1) / (width * 1.05),
        child: BlocBuilder<HomeExploreCubit, HomeExploreState>(
          buildWhen: (p, c) => c is! ExploreGetCategorySuccess,
          builder: (context, state) {
            if (state is HomeExploreSuccess) {
              return LoopPageView.builder(
                controller: pageController,
                allowImplicitScrolling: true,
                itemCount: state.exploreList.length,
                onPageChanged: (page) {
                  ///Purely for UI purpose so just use setState, if need to be
                  ///testable then can use cubit
                  setState(() {
                    activePage = page;
                  });
                },
                itemBuilder: (_, itemIndex) {
                  final margin = itemIndex == activePage ? 0.0 : 32.0;

                  return AnimatedBuilder(
                    animation: pageController,
                    builder: (context, child) {
                      return child!;
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 250),
                      curve: Curves.easeInOutCubic,
                      margin: EdgeInsets.all(margin),
                      child: ExploreSliderItemWidget(
                        item: state.exploreList[itemIndex],
                      ),
                    ),
                  );
                },
              );
            } else if (state is HomeExploreError) {
              return const Center(
                child: Text('Error'),
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

//endregion

//region Use Carrousel Slider
  Widget _buildResponsiveCarrouselSlider() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final w = constraints.maxWidth;
        if (w < 1000) {
          return _buildCarrouselSlider(w);
        } else {
          return Row(
            children: [
              Expanded(
                child: Icon(Icons.arrow_back),
              ),
              Expanded(
                flex: 8,
                child: _buildCarrouselSlider(w),
              ),
              Expanded(
                child: Icon(Icons.arrow_forward),
              ),
            ],
          );
        }
      },
    );
  }

  Widget _buildCarrouselSlider(double width) {
    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.touch,
          PointerDeviceKind.mouse,
        },
      ),
      child: BlocBuilder<HomeExploreCubit, HomeExploreState>(
        buildWhen: (p, c) => c is! ExploreGetCategorySuccess,
        builder: (context, state) {
          if (state is HomeExploreSuccess) {
            return CarouselSlider(
              options: CarouselOptions(
                aspectRatio: (width / 1) / (width * 1.05),
                viewportFraction: 0.75,
                enlargeCenterPage: true,
              ),
              items: state.exploreList.map((item) {
                return Builder(
                  builder: (BuildContext context) {
                    return ExploreSliderItemWidget(
                      item: item,
                    );
                  },
                );
              }).toList(),
            );
          } else if (state is HomeExploreError) {
            return const Center(
              child: Text('Error'),
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
//endregion
}
