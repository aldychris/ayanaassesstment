import 'package:auto_route/annotations.dart';
import 'package:ayana_stg/core/di/dependency_inversion.dart';
import 'package:ayana_stg/core/themes/ayana_colors.dart';
import 'package:ayana_stg/presentation/app/view/appbar/ayana_app_bar.dart';
import 'package:ayana_stg/presentation/home/bloc/home_explore_cubit.dart';
import 'package:ayana_stg/presentation/home/view/explore_carrousel_widget.dart';
import 'package:ayana_stg/presentation/home/view/home_menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unicons/unicons.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AyanaAppBar(
        automaticallyImplyLeading: true,
        title: 'A Y A N A',
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        primary: true,
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(20),
              color: AyanaColors.secondaryAccent,
              child: _buildHomeMenu(),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.only(
                top: 32,
                bottom: 48,
              ),
              color: Colors.white,
              child: _buildTitleText(),
            ),
          ),
          SliverFillRemaining(
            child: Container(
              width: double.infinity,
              color: Colors.white,
              child: BlocProvider<HomeExploreCubit>(
                create: (context) => sl.get<HomeExploreCubit>(),
                child: const ExploreCarrouselWidget(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHomeMenu() {
    return Wrap(
      alignment: WrapAlignment.center,
      runSpacing: 16,
      spacing: 16,
      children: [
        HomeMenuWidget(
          icon: UniconsLine.restaurant,
          title: 'Dining',
          callback: () {},
        ),
        HomeMenuWidget(
          icon: UniconsLine.shop,
          title: 'Spa',
          callback: () {},
        ),
        HomeMenuWidget(
          icon: UniconsLine.sunset,
          title: 'Experience',
          callback: () {},
        ),
        HomeMenuWidget(
          icon: UniconsLine.metro,
          title: 'Tram',
          callback: () {},
        ),
        HomeMenuWidget(
          icon: UniconsLine.bed_double,
          title: 'Room Services',
          callback: () {},
        ),
      ],
    );
  }

  Widget _buildTitleText() {
    return const Column(
      children: [
        Text(
          'Get Inspired',
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
            color: Color(0xFF6C5001),
          ),
        ),
        SizedBox(height: 16),
        Text(
          "Based on what's trending right now",
          style: TextStyle(
            fontSize: 18,
            color: Color(0xFF585248),
          ),
        ),
      ],
    );
  }
}
