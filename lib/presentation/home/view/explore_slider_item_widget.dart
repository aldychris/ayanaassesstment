import 'package:ayana_stg/core/model/home/explore_model.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ExploreSliderItemWidget extends StatelessWidget {
  const ExploreSliderItemWidget({required this.item, super.key});

  final ExploreModel item;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: ExtendedImage.network(
            item.imgUrl,
            fit: BoxFit.cover,
            shape: BoxShape.rectangle,
            border: Border.all(
              width: 0.5,
              color: Colors.grey.shade50,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 32,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  item.title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16),
                Text(
                  item.subtitle,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.sp,
                    height: 1.5,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
