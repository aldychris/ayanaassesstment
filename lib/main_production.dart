import 'package:ayana_stg/bootstrap.dart';
import 'package:ayana_stg/presentation/app/view/app.dart';

void main() {
  bootstrap(() => const App());
}
