import 'package:ayana_stg/bootstrap.dart';
import 'package:ayana_stg/presentation/app/app.dart';

void main() {
  bootstrap(() => const App());
}
