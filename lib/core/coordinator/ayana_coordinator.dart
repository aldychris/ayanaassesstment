import 'package:auto_route/auto_route.dart';
import 'package:ayana_stg/core/coordinator/coordinator.dart';
import 'package:flutter/material.dart';

///Navigation using AutoRoute
class AyanaCoordinator implements Coordinator {
  AyanaCoordinator() : super();

  @override
  bool canPop(BuildContext context) {
    return AutoRouter.of(context).canNavigateBack;
  }

  @override
  void pop<T extends Object?>(BuildContext context, [T? result]) {
    AutoRouter.of(context).pop(result);
  }

  @override
  void pushWidget(BuildContext context, Widget widget) {
    AutoRouter.of(context).pushWidget(widget);
  }
}
