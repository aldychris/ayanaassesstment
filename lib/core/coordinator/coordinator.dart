import 'package:flutter/material.dart';

/// This is abstraction foor navigation system
/// Coordinator is a class that is responsible for navigation between page and
/// also responsible for navigation validation
abstract class Coordinator {
  bool canPop(BuildContext context);
  void pop<T extends Object?>(BuildContext context, [T? result]);
  void pushWidget(BuildContext context, Widget widget);
}
