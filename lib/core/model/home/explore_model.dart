import 'package:equatable/equatable.dart';

class ExploreModel extends Equatable {
  const ExploreModel({
    required this.imgUrl,
    required this.title,
    required this.subtitle,
  });

  final String imgUrl;
  final String title;
  final String subtitle;

  @override
  List<Object?> get props => [imgUrl, title, subtitle];
}
