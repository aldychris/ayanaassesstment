import 'package:equatable/equatable.dart';

class CategoryModel extends Equatable {
  const CategoryModel({required this.name, required this.selected});

  final String name;
  final bool selected;

  @override
  List<Object?> get props => [name, selected];

  CategoryModel copyWith({required bool selected}) {
    return CategoryModel(
      name: name,
      selected: selected,
    );
  }
}
