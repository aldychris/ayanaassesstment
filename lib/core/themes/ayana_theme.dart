import 'package:flutter/material.dart';

enum AyanaThemeKeys { light, dark }

class AyanaThemeData {
  static ThemeData lightTheme = lightThemeData;

  static ThemeData lightThemeData = ThemeData(
    useMaterial3: false,
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      },
    ),
  );
}
