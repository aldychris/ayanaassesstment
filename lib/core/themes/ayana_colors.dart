import 'package:flutter/material.dart';

class AyanaColors {
  static const Color primary = Color(0xffE0E7F8);
  static const Color secondary = Color(0xffCCD4ED);
  static const Color accent = Color(0xff8386B2);
  static const Color secondaryAccent = Color(0xffF2EBE8);
  static const Color textPrimary = Color(0xff5E59B2);
  static const Color textSecondary = Color(0xff2e3338);
  static const Color tint = Color(0xffDEFA1E);
  static const Color tintAccent = Color(0xff38C6F5);
  static const Color error = Color(0xffF5A638);
}
