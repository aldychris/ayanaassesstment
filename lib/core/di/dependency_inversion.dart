import 'package:ayana_stg/core/coordinator/index.dart';
import 'package:ayana_stg/core/routes/ayana_router.dart';
import 'package:ayana_stg/core/routes/gurad/auth_guard.dart';
import 'package:ayana_stg/core/utils/device_screen_helper.dart';
import 'package:ayana_stg/presentation/app/bloc/index.dart';
import 'package:ayana_stg/presentation/home/bloc/home_explore_cubit.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

void setupGetIt() {
  sl
    ..registerSingleton(DeviceScreenHelper())
    ..registerSingleton(UserAuthCubit())
    ..registerSingleton<Coordinator>(AyanaCoordinator())
    ..registerSingleton(RootBloc())
    ..registerSingleton(AuthGuard(FakeUserData(), sl.get<UserAuthCubit>()))
    ..registerSingleton(AyanaRouter(authGuard: sl.get<AuthGuard>()))
    ..registerFactory(HomeExploreCubit.new);
}
