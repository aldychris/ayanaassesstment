import 'package:auto_route/auto_route.dart';
import 'package:ayana_stg/core/routes/ayana_router.gr.dart';
import 'package:ayana_stg/presentation/app/bloc/user_auth_cubit.dart';

class FakeUserData {
  bool isSignedIn() => false;
}

class AuthGuard extends AutoRouteGuard {
  AuthGuard(this.userData, this.userAuthCubit) {
    userAuthCubit.stream.listen((state) {
      if (state is LoginSuccess) {
        print('LoginSuccess');
      }
    });
  }

  final FakeUserData userData;
  final UserAuthCubit userAuthCubit;

  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    if (userData.isSignedIn()) {
      return resolver.next();
    }
    router.push(const HomeRoute());
  }
}
