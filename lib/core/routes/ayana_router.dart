import 'package:auto_route/auto_route.dart';
import 'package:ayana_stg/core/routes/ayana_router.gr.dart';
import 'package:ayana_stg/core/routes/gurad/auth_guard.dart';

@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
  deferredLoading: true,
)
class AyanaRouter extends $AyanaRouter {
  AyanaRouter({required this.authGuard});

  late AuthGuard authGuard;

  @override
  RouteType get defaultRouteType => const RouteType.adaptive();

  @override
  List<AutoRoute> get routes => [
        // AutoRoute<dynamic>(path: '*', page: UnknownPage),
        AutoRoute(
          page: RootRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: HomeRoute.page,
        ),
        AutoRoute(
          page: ChatRoute.page,
          guards: [authGuard],
        ),
        AutoRoute(page: MapRoute.page),
        AutoRoute(page: ProfileRoute.page),
      ];
}
